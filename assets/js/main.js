$(function() {

    "use strict";


    //===== Prealoder

    $(window).on('load', function(event) {
        $('.preloader').delay(500).fadeOut(500);
    });


    //===== Mobile Menu 

    $(".navbar-toggler").on('click', function() {
        $(this).toggleClass('active');
    });

    $(".navbar-nav a").on('click', function() {
        $(".navbar-toggler").removeClass('active');
    });


    //===== close navbar-collapse when a  clicked

    $(".navbar-nav a").on('click', function() {
        $(".navbar-collapse").removeClass("show");
    });


    //===== Sticky

    // $(window).on('scroll', function(event) {
    //     var scroll = $(window).scrollTop();
    //     if (scroll < 10) {
    //         $(".header-area").removeClass("sticky");
    //     } else {
    //         $(".header-area").addClass("sticky");
    //     }
    // });


    //===== One Page Nav

    $('#nav').onePageNav({
        filter: ':not(.external)',
        currentClass: 'active',
    });


    //=====  Slick

    function mainSlider() {
        var BasicSlider = $('.slider-active');
        BasicSlider.on('init', function(e, slick) {
            var $firstAnimatingElements = $('.single-slider:first-child').find('[data-animation]');
            doAnimations($firstAnimatingElements);
        });
        BasicSlider.on('beforeChange', function(e, slick, currentSlide, nextSlide) {
            var $animatingElements = $('.single-slider[data-slick-index="' + nextSlide + '"]').find('[data-animation]');
            doAnimations($animatingElements);
        });
        BasicSlider.slick({
            autoplay: true,
            autoplaySpeed: 5000,
            dots: true,
            fade: true,
            arrows: false,
            responsive: [
                { breakpoint: 767, settings: { dots: false, arrows: false } }
            ]
        });

        function doAnimations(elements) {
            var animationEndEvents = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
            elements.each(function() {
                var $this = $(this);
                var $animationDelay = $this.data('delay');
                var $animationType = 'animated ' + $this.data('animation');
                $this.css({
                    'animation-delay': $animationDelay,
                    '-webkit-animation-delay': $animationDelay
                });
                $this.addClass($animationType).one(animationEndEvents, function() {
                    $this.removeClass($animationType);
                });
            });
        }
    }
    mainSlider();



    //=====  Slick product items active

    $('.product-items-active').slick({
        dots: false,
        infinite: true,
        speed: 600,
        slidesToShow: 3,
        slidesToScroll: 1,
        adaptiveHeight: true,
        arrows: true,
        prevArrow: '<span class="prev"><i class="lni-chevron-left"></i></span>',
        nextArrow: '<span class="next"><i class="lni-chevron-right"></i></span>',
        responsive: [{
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    arrows: false,
                }
            },
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 1,
                    arrows: false,
                }
            }
        ]
    });


    //=====  Slick Showcase active

    $('.showcase-active').slick({
        dots: false,
        infinite: true,
        speed: 600,
        slidesToShow: 1,
        arrows: true,
        prevArrow: '<span class="prev"><i class="lni-arrow-left"></i></span>',
        nextArrow: '<span class="next"><i class="lni-arrow-right"></i></span>',
        adaptiveHeight: true,
        responsive: [{
                breakpoint: 768,
                settings: {
                    arrows: false,
                }
            },
            {
                breakpoint: 576,
                settings: {
                    arrows: false,
                }
            }
        ]
    });


    //=====  Slick testimonial active

    $('.testimonial-active').slick({
        dots: false,
        autoplay: true,
        autoplaySpeed: 2000,
        infinite: true,
        speed: 600,
        slidesToShow: 1,
        arrows: false,
        adaptiveHeight: true,
    });


    //====== Magnific Popup

    $('.Video-popup').magnificPopup({
        type: 'iframe'
            // other options
    });


    //===== Back to top

    // Show or hide the sticky footer button
    $(window).on('scroll', function(event) {
        if ($(this).scrollTop() > 600) {
            $('.back-to-top').fadeIn(200)
        } else {
            $('.back-to-top').fadeOut(200)
        }
    });

    //Animate the scroll to yop
    $('.back-to-top').on('click', function(event) {
        event.preventDefault();

        $('html, body').animate({
            scrollTop: 0,
        }, 1500);
    });


    //====== Slick product image

    $('.product-image').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        prevArrow: '<span class="prev"><i class="lni-chevron-left"></i></span>',
        nextArrow: '<span class="next"><i class="lni-chevron-right"></i></i></span>',
        dots: false,
    });


    //====== Nice Number

    $('input[type="number"]').niceNumber({

    });


    //=====  Rating selection

    var $star_rating = $('.star-rating .fa');

    var SetRatingStar = function() {
        return $star_rating.each(function() {
            if (parseInt($star_rating.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
                return $(this).removeClass('fa-star-o').addClass('fa-star');
            } else {
                return $(this).removeClass('fa-star').addClass('fa-star-o');
            }
        });
    };

    $star_rating.on('click', function() {
        $star_rating.siblings('input.rating-value').val($(this).data('rating'));
        return SetRatingStar();
    });

    SetRatingStar();


    // main slide swiper
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        paginationClickable: true,
        parallax: true,
        speed: 600,
        effect: 'fade',
        slidesPerView: 'auto',
        loop: true,
        autoplay: 6000,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        }
    });
    // menu add gray bg and esc to collapse 
    $(window).scroll(function() {

        let oppai = $(this).scrollTop();

        $('#content article').css({
            opacity: 100 / oppai,
            filter: 'blur(' + oppai / 100 + 'px)'
        });
        $('#content').css({
            opacity: 100 / oppai
        });

        if (oppai > 190) {
            if (!$('body').hasClass('abrido'))
                $('#header-main').addClass('arre');
        } else {
            $('#header-main').removeClass('arre');
        }
    });

    $('#burger').on('click', function(e) {

        e.preventDefault();

        $('#nav-main, body, #burger').toggleClass('abrido');
        // $('.black-logo').removeClass('show-logo').addClass('hide-logo');
        // $('.white-logo').removeClass('hide-logo').addClass('show-logo');
        if ($('#header-main').hasClass('arre')) {
            $('#header-main').removeClass('arre').addClass('arreno');
        } else if ($('#header-main').hasClass('arreno')) {
            $('#header-main').removeClass('arreno');
            setTimeout(() => {
                $('#header-main').addClass('arre')
            }, 800);
        }

    });
    $(document).keyup(function(e) {
        if (e.keyCode == 27) { // escape key maps to keycode `27`
            e.preventDefault();
            if ($('#nav-main').hasClass('abrido')) {
                $('#nav-main, body, #burger').toggleClass('abrido');
            }
        }
    });
     /*
		variables
	*/

		var $imagesSlider = $(".gallery-slider .gallery-slider__images>div"),
        $thumbnailsSlider = $(".gallery-slider__thumbnails>div");

/*
  sliders
*/

  // images options
  $imagesSlider.slick({
      speed:300,
      slidesToShow:1,
      slidesToScroll:1,
      cssEase:'linear',
      fade:true,
      draggable:false,
      asNavFor:".gallery-slider__thumbnails>div",
      prevArrow:'.gallery-slider__images .prev-arrow',
      nextArrow:'.gallery-slider__images .next-arrow'
  });

  // thumbnails options
  $thumbnailsSlider.slick({
      speed:300,
      slidesToShow:5,
      slidesToScroll:1,
      cssEase:'linear',
      centerMode:true,
      draggable:false,
      focusOnSelect:true,
      asNavFor:".gallery-slider .gallery-slider__images>div",
      prevArrow:'.gallery-slider__thumbnails .prev-arrow',
      nextArrow:'.gallery-slider__thumbnails .next-arrow',
      responsive: [
          {
              breakpoint: 720,
              settings: {
                  slidesToShow: 4,
                  slidesToScroll: 4
              }
          },
          {
              breakpoint: 576,
              settings: {
                  slidesToShow: 3,
                  slidesToScroll: 3
              }
          },
          {
              breakpoint: 350,
              settings: {
                  slidesToShow: 2,
                  slidesToScroll: 2
              }
          }
      ]
  });

/* 
  captions
*/

  var $caption = $('.gallery-slider .caption');

  // get the initial caption text
  var captionText = $('.gallery-slider__images .slick-current img').attr('alt');
  updateCaption(captionText);

  // hide the caption before the image is changed
  $imagesSlider.on('beforeChange', function(event, slick, currentSlide, nextSlide){
      $caption.addClass('hide');
  });

  // update the caption after the image is changed
  $imagesSlider.on('afterChange', function(event, slick, currentSlide, nextSlide){
      captionText = $('.gallery-slider__images .slick-current img').attr('alt');
      updateCaption(captionText);
  });

  function updateCaption(text) {
      // if empty, add a no breaking space
      if (text === '') {
          text = '&nbsp;';
      }
      $caption.html(text);
      $caption.removeClass('hide');
  }

    // -------- modal open image ---------
    //   // TODO: touch events

    // const divs = document.getElementsByClassName("ex-div");
    // console.log(divs);
    // const body = document.body;
    // const prev = document.querySelector('.prev');
    // const next = document.querySelector('.next');
    // const first = document.querySelector('.ex-div:first-child');
    // const last = document.querySelector('.ex-div:last-child');
    // console.log(first);
    // checkPrev = () => $(first).classList.contains('show') ? prev.style.display = 'none' : prev.style.display = 'flex';

    // checkNext = () => $(last).classList.contains('show') ? next.style.display = 'none' : next.style.display = 'flex';

    // Array.prototype.slice.call(divs).forEach(function(el) {
    //     el.addEventListener('click', function() {
    //         this.classList.toggle('show');
    //         body.classList.toggle('active');
    //         checkNext();
    //         checkPrev();
    //     });
    // });

    // prev.addEventListener('click', function() {
    //     const show = document.querySelector('.show');
    //     const event = document.createEvent('HTMLEvents');
    //     event.initEvent('click', true, false);

    //     show.previousElementSibling.dispatchEvent(event);
    //     show.classList.remove('show');
    //     body.classList.toggle('active');
    //     checkNext();
    // });

    // next.addEventListener('click', function() {
    //     const show = document.querySelector('.show');
    //     const event = document.createEvent('HTMLEvents');
    //     event.initEvent('click', true, false);

    //     show.nextElementSibling.dispatchEvent(event);
    //     show.classList.remove('show');
    //     body.classList.toggle('active');
    //     checkPrev();
    // });
    // sticky side right menu
    $(document).ready(function() {
        var $window = $(window);
        var $rightbar = $(".right-ctn");
        var $rightbarHeight = $rightbar.innerHeight();
        var $footerOffsetTop = $(".footer").offset().top;
        var $rightbarOffset = $rightbar.offset();

        $window.scroll(function() {
            if ($window.scrollTop() > $rightbarOffset.top) {
                $rightbar.addClass("fixed");
            } else {
                $rightbar.removeClass("fixed");
            }
            if ($window.scrollTop() + $rightbarHeight > $footerOffsetTop) {
                $rightbar.css({ "top": -($window.scrollTop() + $rightbarHeight - $footerOffsetTop) });
            } else {
                $rightbar.css({ "top": "0", });
            }
        });


    });


});